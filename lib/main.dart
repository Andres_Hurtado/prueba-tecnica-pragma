import 'package:catbreeds/dependencyInjection/dependecy_injection.dart';
import 'package:catbreeds/screens/home/home_screen.dart';
import 'package:catbreeds/shared/routes.dart';
import 'package:catbreeds/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureInjection();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: ligthTheme.colorScheme.primary,
  ));
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PRUEBA TECNICA PRAMA',
      theme: ligthTheme.copyWith(useMaterial3: true),
      initialRoute: HomeScreen.routeName,
      onGenerateRoute: onGenerateRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}
