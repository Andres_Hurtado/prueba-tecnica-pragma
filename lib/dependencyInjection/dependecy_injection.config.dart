// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:catbreeds/screens/home/bloc/cat/cat_bloc.dart' as _i3;
import 'package:catbreeds/screens/search/bloc/cat_search/cat_search_bloc.dart'
    as _i5;
import 'package:domain/domain.dart' as _i4;
import 'package:domain/domain.module.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:infrastructure/infrastructure.module.dart' as _i6;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
  // initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.CatBloc>(
        () => _i3.CatBloc(catServices: gh<_i4.CatServices>()));
    gh.factory<_i5.CatSearchBloc>(
        () => _i5.CatSearchBloc(catServices: gh<_i4.CatServices>()));
    await _i6.InfrastructurePackageModule().init(gh);
    await _i7.DomainPackageModule().init(gh);
    return this;
  }
}
