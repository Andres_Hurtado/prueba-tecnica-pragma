import 'package:catbreeds/screens/search/bloc/cat_search/cat_search_bloc.dart';
import 'package:catbreeds/screens/search/debouncer/debouncer.dart';
import 'package:catbreeds/shared/widget/custom_input_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InputSearch extends StatefulWidget {
  const InputSearch({super.key});

  @override
  State<InputSearch> createState() => _InputSearchState();
}

class _InputSearchState extends State<InputSearch> {
  late final TextEditingController txtSearch;
  late Debouncer _debouncer;

  @override
  void initState() {
    _debouncer = Debouncer();
    txtSearch = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    txtSearch
      ..clear()
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      color: Theme.of(context).colorScheme.primary,
      child: CustomTextFormField(
        controller: txtSearch,
        hintText: 'Buscar raza',
        textAlign: TextAlign.start,
        padding: 10,
        autofocus: false,
        onChanged: (value) => _debouncer.run(
          () async => context
              .read<CatSearchBloc>()
              .add(SearchCatBreeds(razaIdName: value)),
        ),
      ),
    );
  }
}
