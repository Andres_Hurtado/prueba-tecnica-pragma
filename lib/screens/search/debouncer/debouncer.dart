import 'dart:async';
import 'package:flutter/foundation.dart';

class Debouncer {
  Timer? _timer;
  void run(VoidCallback action) {
    _timer?.cancel();
    _timer = Timer(const Duration(milliseconds: 500), action);
  }
}
