import 'package:catbreeds/screens/search/bloc/cat_search/cat_search_bloc.dart';
import 'package:catbreeds/screens/search/widget/search_cat_widget.dart';
import 'package:catbreeds/shared/widget/custom_loading.dart';
import 'package:catbreeds/shared/widget/detail_cat_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});
  static const routeName = 'search-screen';

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: AppBar(
          leading: BackButton(
            color: Theme.of(context).colorScheme.surface,
          ),
          titleSpacing: 0,
          backgroundColor: Theme.of(context).colorScheme.primary,
          title: const InputSearch(),
        ),
      ),
      body: BlocBuilder<CatSearchBloc, CatSearchState>(
        builder: (context, state) {
          Widget body = const SizedBox.shrink();
          if (state is CatSearchLoading) {
            body = const CustomLoading();
          }
          if (state is CatSearchLoaded) {
            if (state.cat != null) {
              body = Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: DetailCardCatBreed(cat: state.cat!),
              );
            } else {
              body = const _DataEmpty();
            }
          }
          return SafeArea(
            child: SizedBox(width: w, height: 400, child: body),
          );
        },
      ),
    );
  }
}

class _DataEmpty extends StatelessWidget {
  const _DataEmpty();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
