part of 'cat_search_bloc.dart';

abstract class CatSearchEvent extends Equatable {
  const CatSearchEvent();

  @override
  List<Object> get props => [];
}

class SearchCatBreeds extends CatSearchEvent {
  const SearchCatBreeds({required this.razaIdName});
  final String razaIdName;
  @override
  List<Object> get props => [razaIdName];
}
