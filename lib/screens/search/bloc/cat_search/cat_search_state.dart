part of 'cat_search_bloc.dart';

abstract class CatSearchState extends Equatable {
  const CatSearchState();

  @override
  List<Object> get props => [];
}

class CatSearchInitial extends CatSearchState {}

class CatSearchLoading extends CatSearchState {}

class CatSearchLoaded extends CatSearchState {
  const CatSearchLoaded(this.cat);
  final CatModel? cat;
}

class CatSearchErrror extends CatSearchState {
  const CatSearchErrror(this.error);
  final String error;
  @override
  List<Object> get props => [error];
}
