import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';

part 'cat_search_event.dart';
part 'cat_search_state.dart';

@injectable
class CatSearchBloc extends Bloc<CatSearchEvent, CatSearchState> {
  CatSearchBloc({required CatServices catServices})
      : _catServices = catServices,
        super(CatSearchInitial()) {
    on<SearchCatBreeds>(_onSearchCatBreeds);
  }
  final CatServices _catServices;

  Future<void> _onSearchCatBreeds(
      SearchCatBreeds event, Emitter<CatSearchState> emit) async {
    emit(CatSearchLoading());
    try {
      if (event.razaIdName.isEmpty) emit(CatSearchInitial());
      final cat = await _catServices.searchCatBreed(event.razaIdName);
      emit(CatSearchLoaded(cat));
    } on NotConnectedToNetworkException catch (e) {
      emit(CatSearchErrror(e.message));
    } on HttpClientException catch (e) {
      emit(CatSearchErrror(e.message));
    } on ApiKeyNoFoundException catch (e) {
      emit(CatSearchErrror(e.message));
    } catch (e) {
      final message = e.toString();
      emit(CatSearchErrror(message));
    }
  }
}
