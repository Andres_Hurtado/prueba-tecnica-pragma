import 'package:catbreeds/screens/search/search_screen.dart';
import 'package:catbreeds/shared/widget/custom_input_text.dart';
import 'package:flutter/material.dart';

class InputSearchHome extends StatelessWidget {
  const InputSearchHome({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      color: Theme.of(context).colorScheme.primary,
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, SearchScreen.routeName),
        child: const CustomTextFormField(
          hintText: 'Buscar raza',
          textAlign: TextAlign.start,
          padding: 10,
          autofocus: false,
          enabled: false,
        ),
      ),
    );
  }
}
