part of 'cat_bloc.dart';

abstract class CatState extends Equatable {
  @override
  List<Object?> get props => [];
}

class CatsInitial extends CatState {}

class CatsLoading extends CatState {
  CatsLoading(this.oldCats, {required this.isFirstFetch});
  final List<CatModel> oldCats;
  final bool isFirstFetch;
  @override
  List<Object?> get props => [oldCats, isFirstFetch];
}

class CatsLoaded extends CatState {
  CatsLoaded(this.cats);
  final List<CatModel> cats;
  @override
  List<Object?> get props => [cats];
}

class CatsErrror extends CatState {
  CatsErrror(this.error);
  final String error;
  @override
  List<Object?> get props => [error];
}
