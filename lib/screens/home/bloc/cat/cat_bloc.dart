import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:infrastructure/infrastructure.dart';
import 'package:injectable/injectable.dart';

part 'cat_event.dart';
part 'cat_state.dart';

@injectable
class CatBloc extends Bloc<CatEvent, CatState> {
  CatBloc({required CatServices catServices})
      : _catServices = catServices,
        super(CatsInitial()) {
    on<GetCatBreeds>(_onGetBreedsEvent);
  }

  final CatServices _catServices;
  int page = 1;

  Future<void> _onGetBreedsEvent(
    GetCatBreeds event,
    Emitter<CatState> emit,
  ) async {
    if (state is CatsLoading) return;
    final currentState = state;
    var oldCats = <CatModel>[];
    if (currentState is CatsLoaded) {
      oldCats = currentState.cats;
    }

    emit(CatsLoading(oldCats, isFirstFetch: page == 1));
    try {
      final newCats = await _catServices.getCatBreeds(page);
      page++;
      final cats = (state as CatsLoading).oldCats..addAll(newCats);
      emit(CatsLoaded(cats));
    } on NotConnectedToNetworkException catch (e) {
      emit(CatsErrror(e.message));
    } on HttpClientException catch (e) {
      emit(CatsErrror(e.message));
    } on ApiKeyNoFoundException catch (e) {
      emit(CatsErrror(e.message));
    } catch (e) {
      final message = e.toString();
      emit(CatsErrror(message));
    }
  }
}
