import 'package:catbreeds/screens/home/bloc/cat/cat_bloc.dart';
import 'package:catbreeds/screens/home/widget/search_cat_widget.dart';
import 'package:catbreeds/shared/widget/custom_loading.dart';
import 'package:catbreeds/shared/widget/detail_cat_widget.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  static const routeName = 'home-screen';

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final ScrollController scrollCOntroller;

  @override
  void initState() {
    scrollCOntroller = ScrollController();
    setupScrollController();
    super.initState();
  }

  void setupScrollController() {
    scrollCOntroller.addListener(() {
      if (scrollCOntroller.position.atEdge) {
        if (scrollCOntroller.position.pixels != 0) {
          context.read<CatBloc>().add(GetCatBreeds());
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;

    return Scaffold(
      body: BlocBuilder<CatBloc, CatState>(
        builder: (context, state) {
          Widget body = const SizedBox.shrink();
          if (state is CatsLoading && state.isFirstFetch) {
            body = const CustomLoading();
          }
          var cats = <CatModel>[];
          var isLoading = false;

          if (state is CatsLoading) {
            cats = state.oldCats;
            isLoading = true;
          } else if (state is CatsLoaded) {
            cats = state.cats;
          }

          if (cats.isNotEmpty) {
            body = _CatBreedsLIst(
              cats,
              controller: scrollCOntroller,
              isLoading: isLoading,
            );
          } else if (state is CatsLoading && !state.isFirstFetch) {
            body = const _DataEmpty();
          }

          return SafeArea(
            child: SizedBox(
              width: w,
              height: h,
              child: Column(
                children: [
                  InputSearchHome(),
                  Expanded(child: body),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class _CatBreedsLIst extends StatelessWidget {
  const _CatBreedsLIst(this.cats,
      {required this.isLoading, required this.controller});
  final List<CatModel> cats;
  final bool isLoading;
  final ScrollController controller;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      controller: controller,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      itemBuilder: (context, index) {
        if (index < cats.length) {
          return DetailCardCatBreed(cat: cats[index]);
        } else {
          return const CustomLoading();
        }
      },
      separatorBuilder: (context, index) => const _DividerCard(),
      itemCount: cats.length + (isLoading ? 1 : 0),
    );
  }
}

class _DividerCard extends StatelessWidget {
  const _DividerCard();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class _DataEmpty extends StatelessWidget {
  const _DataEmpty();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
