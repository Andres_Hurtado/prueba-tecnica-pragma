import 'package:catbreeds/shared/widget/cat_not_image.dart';
import 'package:catbreeds/shared/widget/custom_cached_image.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen(this.cat, {super.key});
  static const routeName = 'detail-screen';
  final CatModel cat;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          cat.id,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        leading: BackButton(
          color: Theme.of(context).colorScheme.surface,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (cat.referenceImage != null && cat.referenceImage!.isNotEmpty)
              CustomCachedImage(
                url: cat.referenceImage!,
                borderRadius: BorderRadius.circular(0),
              ),
            if (cat.referenceImage == null || cat.referenceImage!.isEmpty)
              const CatNotImage(),
            _ContentBody(cat)
          ],
        ),
      ),
    );
  }
}

class _ContentBody extends StatelessWidget {
  const _ContentBody(this.cat);
  final CatModel cat;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(cat.description),
          const SizedBox(height: 10),
          _Content(title: 'Name', content: cat.name),
          _Content(title: 'Origin', content: cat.origin),
          _Content(title: 'Adaptability', content: cat.adaptability.toString()),
          _Content(title: 'Temperament', content: cat.temperament),
          _Content(
              title: 'Stranger Friendly',
              content: cat.strangerFriendly.toString()),
          _Content(title: 'Dog Friendly', content: cat.dogFriendly.toString()),
          _Content(title: 'Energy Level', content: cat.energyLevel.toString()),
          _Content(title: 'Intelligence', content: cat.intelligence.toString()),
          _Content(
              title: 'Hypoallergenic', content: cat.hypoallergenic.toString()),
          _Content(
              title: 'Affection Level', content: cat.affectionLevel.toString()),
        ],
      ),
    );
  }
}

class _TitleContent extends StatelessWidget {
  const _TitleContent({
    required this.title,
  });
  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: Theme.of(context)
            .textTheme
            .bodyMedium!
            .copyWith(fontWeight: FontWeight.bold));
  }
}

class _Content extends StatelessWidget {
  const _Content({
    required this.title,
    required this.content,
  });
  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _TitleContent(title: title),
        Text(content),
        const SizedBox(height: 10),
      ],
    );
  }
}
