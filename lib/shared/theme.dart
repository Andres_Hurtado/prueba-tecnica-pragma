import 'package:flutter/material.dart';

const kFontPoppinsRegular = 'Poppins Regular';
const kFontPoppinsLightItalic = 'Poppins LightItalic';

class CatColors {
  static const int _primaryColor = 0xff6750a4;
  static const primaryColor = MaterialColor(_primaryColor, {
    100: Color(0xff22005d),
    200: Color(0xFF381e72),
    250: Color(0xFF432b7e),
    300: Color(0xFF4f378a),
    350: Color(0xFF5b4397),
    400: Color(_primaryColor),
    500: Color(0xFF8069bf),
    600: Color(0xFF9a83db),
    700: Color(0xFFb69df8),
    800: Color(0xFFcfbcff),
    900: Color(0xFFe9ddff),
    950: Color(0xFFf6eeff),
    980: Color(0xFFfdf7ff),
    990: Color(0xFFfffbff),
    1000: Color(0xFFffffff),
  });
  static const int _secondaryColor = 0xFF625b71;
  static const secondaryColor = MaterialColor(_secondaryColor, {
    100: Color(0xff1e192b),
    200: Color(0xFF332d41),
    250: Color(0xFF3e384c),
    300: Color(0xFF4a4458),
    350: Color(0xFF564f64),
    400: Color(_secondaryColor),
    500: Color(0xFF7b748a),
    600: Color(0xFF958da4),
    700: Color(0xFFb0a7c0),
    800: Color(0xFFcbc2db),
    900: Color(0xFFe8def8),
    950: Color(0xFFf6eeff),
    980: Color(0xFFfdf7ff),
    990: Color(0xFFfffbff),
    1000: Color(0xFFffffff),
  });
  static const backgroundColor = Color(0xfffffbff);
  static const error = Color(0xffba1a1a);
  static const redColor = Color(0xffba1a1a);
  static const whiteColor = Color(0xffffffff);
  static const blackColor = Color(0xff000000);
  static const greyColor = Color(0xFFCDCDCD);

  static const int _tertiaryColor = 0xFF7e5260;
  static const tertiaryColor = MaterialColor(_tertiaryColor, {
    400: Color(_tertiaryColor),
    500: Color(0xff996a79),
    600: Color(0xFFb58392),
    700: Color(0xFFd29dad),
    800: Color(0xFFefb8c8),
    900: Color(0xffffd9e3),
    950: Color(0xFFffecf0),
    980: Color(0xFFfff8f8),
    990: Color(0xFFfffbff),
    1000: Color(0xFFffffff),
  });
}

final ligthTheme = ThemeData(
  scaffoldBackgroundColor: CatColors.backgroundColor,
  iconTheme: IconThemeData(color: CatColors.tertiaryColor[400]),
  colorScheme:
      ColorScheme.fromSwatch(primarySwatch: CatColors.primaryColor).copyWith(
    primary: CatColors.primaryColor,
    secondary: CatColors.secondaryColor,
    tertiary: CatColors.tertiaryColor,
    background: CatColors.backgroundColor,
    surface: CatColors.whiteColor,
    error: CatColors.error,
    shadow: CatColors.primaryColor[600],
  ),
  appBarTheme: AppBarTheme(
    elevation: 0,
    titleTextStyle: const TextStyle(
      color: CatColors.whiteColor,
      fontSize: 16,
      fontFamily: kFontPoppinsRegular,
    ),
    backgroundColor: CatColors.backgroundColor,
    iconTheme: IconThemeData(color: CatColors.tertiaryColor[400], size: 35),
  ),
  textTheme: const TextTheme(
    displayLarge: TextStyle(
      fontSize: 28,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    ),
    displayMedium: TextStyle(
      fontSize: 24,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.18,
    ),
    displaySmall: TextStyle(
      fontSize: 20,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.bold,
    ),
    headlineSmall: TextStyle(
      fontSize: 24,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.bold,
    ),
    titleLarge: TextStyle(
      fontSize: 20,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.bold,
    ),
    titleMedium: TextStyle(
      fontSize: 16,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.normal,
      color: Color(0xB3FFFFFF),
    ),
    titleSmall: TextStyle(
      fontSize: 14,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.normal,
    ),
    bodyLarge: TextStyle(
      fontSize: 16,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.normal,
    ),
    bodyMedium: TextStyle(
      fontSize: 14,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.normal,
    ),
    labelLarge: TextStyle(
      fontSize: 16,
      fontFamily: kFontPoppinsLightItalic,
      fontWeight: FontWeight.bold,
      letterSpacing: 1.42,
      color: CatColors.tertiaryColor,
    ),
    bodySmall: TextStyle(
      fontSize: 12,
      fontFamily: kFontPoppinsRegular,
      fontWeight: FontWeight.normal,
    ),
    labelSmall: TextStyle(
        fontSize: 10,
        fontFamily: kFontPoppinsLightItalic,
        fontWeight: FontWeight.w900,
        letterSpacing: 1.5,
        color: CatColors.tertiaryColor),
  ),
);
