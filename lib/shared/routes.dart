import 'package:catbreeds/dependencyInjection/dependecy_injection.dart';
import 'package:catbreeds/screens/detail/detail_screen.dart';
import 'package:catbreeds/screens/home/bloc/cat/cat_bloc.dart';
import 'package:catbreeds/screens/home/home_screen.dart';
import 'package:catbreeds/screens/search/bloc/cat_search/cat_search_bloc.dart';
import 'package:catbreeds/screens/search/search_screen.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

RouteFactory onGenerateRoute = (RouteSettings settings) {
  final name = settings.name;

  if (name == HomeScreen.routeName) {
    const transitionDuration = Duration(milliseconds: 150);

    return PageRouteBuilder(
      transitionDuration: transitionDuration,
      reverseTransitionDuration: transitionDuration,
      pageBuilder: (_, animation, ___) {
        return FadeTransition(
          opacity: animation,
          child: BlocProvider(
            create: (_) => getIt<CatBloc>()..add(GetCatBreeds()),
            child: const HomeScreen(),
          ),
        );
      },
    );
  }

  if (name == SearchScreen.routeName) {
    const transitionDuration = Duration(milliseconds: 150);

    return PageRouteBuilder(
      transitionDuration: transitionDuration,
      reverseTransitionDuration: transitionDuration,
      pageBuilder: (_, animation, ___) {
        return FadeTransition(
          opacity: animation,
          child: BlocProvider(
            create: (context) => getIt<CatSearchBloc>(),
            child: const SearchScreen(),
          ),
        );
      },
    );
  }

  if (name == DetailScreen.routeName) {
    const transitionDuration = Duration(milliseconds: 150);
    final cat = settings.arguments as CatModel;

    return PageRouteBuilder(
      transitionDuration: transitionDuration,
      reverseTransitionDuration: transitionDuration,
      pageBuilder: (_, animation, ___) {
        return FadeTransition(
          opacity: animation,
          child: DetailScreen(cat),
        );
      },
    );
  }

  return null;
};
