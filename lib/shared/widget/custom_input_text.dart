import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    super.key,
    TextEditingController? controller,
    int? maxLines = 1,
    int? minLines = 1,
    TextAlign textAlign = TextAlign.center,
    required String hintText,
    TextInputType keyboardType = TextInputType.text,
    TextInputAction textInputAction = TextInputAction.done,
    bool autofocus = true,
    void Function(String)? onChanged,
    bool enabled = true,
    double fontSize = 16,
    int? maxLength,
    double padding = 0,
  })  : _controller = controller,
        _maxLines = maxLines,
        _minLines = minLines,
        _textAlign = textAlign,
        _keyboardType = keyboardType,
        _hintText = hintText,
        _textInputAction = textInputAction,
        _autofocus = autofocus,
        _onChanged = onChanged,
        _enabled = enabled,
        _fontSize = fontSize,
        _maxLength = maxLength,
        _padding = padding;

  final TextEditingController? _controller;
  final int? _maxLines;
  final int? _minLines;
  final TextAlign _textAlign;
  final String _hintText;
  final TextInputType? _keyboardType;
  final TextInputAction? _textInputAction;
  final bool _autofocus;
  final void Function(String)? _onChanged;
  final bool _enabled;
  final double _fontSize;
  final int? _maxLength;
  final double _padding;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      textAlign: _textAlign,
      maxLines: _maxLines,
      minLines: _minLines,
      autofocus: _autofocus,
      keyboardType: _keyboardType,
      textInputAction: _textInputAction,
      onChanged: _onChanged,
      enabled: _enabled,
      maxLength: _maxLength,
      style: Theme.of(context).textTheme.titleMedium!.copyWith(
            color: Theme.of(context).colorScheme.surface,
            fontSize: _fontSize,
          ),
      decoration: InputDecoration(
        hintText: _hintText,
        hintStyle: TextStyle(color: Theme.of(context).colorScheme.surface),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Theme.of(context).colorScheme.surface),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).colorScheme.surface),
          borderRadius: BorderRadius.circular(10),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Theme.of(context).colorScheme.surface),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: Theme.of(context).colorScheme.surface,
          ),
        ),
        contentPadding: EdgeInsets.only(
          left: _padding,
          right: _padding,
          bottom: _fontSize / 2,
        ),
      ),
    );
  }
}
