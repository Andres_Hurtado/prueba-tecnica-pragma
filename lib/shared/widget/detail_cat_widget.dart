import 'package:catbreeds/screens/detail/detail_screen.dart';
import 'package:catbreeds/shared/widget/cat_not_image.dart';
import 'package:catbreeds/shared/widget/custom_cached_image.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';

class DetailCardCatBreed extends StatelessWidget {
  const DetailCardCatBreed({super.key, required this.cat});

  final CatModel cat;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: Container(
        height: 400,
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _CatPlusButton(cat: cat),
            if (cat.referenceImage != null && cat.referenceImage!.isNotEmpty)
              CustomCachedImage(url: cat.referenceImage!),
            if (cat.referenceImage == null || cat.referenceImage!.isEmpty)
              const CatNotImage(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [const Text('Pais de origen'), Text(cat.origin)],
            )
          ],
        ),
      ),
    );
  }
}

class _CatPlusButton extends StatelessWidget {
  const _CatPlusButton({required this.cat});

  final CatModel cat;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(cat.id),
        InkWell(
          radius: 10,
          child: Container(
            width: 50,
            alignment: Alignment.centerRight,
            child: const Text('Mas'),
          ),
          onTap: () => Navigator.pushNamed(
            context,
            DetailScreen.routeName,
            arguments: cat,
          ),
        ),
      ],
    );
  }
}
