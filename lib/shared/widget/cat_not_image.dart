import 'package:catbreeds/shared/theme.dart';
import 'package:flutter/material.dart';

class CatNotImage extends StatelessWidget {
  const CatNotImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 300,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: CatColors.greyColor.withOpacity(0.4),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: const Text('Imagen no disponible'),
    );
  }
}
