//@GeneratedMicroModule;InfrastructurePackageModule;package:infrastructure/infrastructure.module.dart
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i2;

import 'package:domain/domain.dart' as _i6;
import 'package:infrastructure/src/cat/repository/cat_http_repository.dart'
    as _i7;
import 'package:infrastructure/src/shared/http_client/http_client.dart' as _i3;
import 'package:infrastructure/src/shared/network/network_module.dart' as _i8;
import 'package:infrastructure/src/shared/network/network_verify.dart' as _i5;
import 'package:injectable/injectable.dart' as _i1;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i4;

class InfrastructurePackageModule extends _i1.MicroPackageModule {
  // initializes the registration of main-scope dependencies inside of GetIt
  @override
  _i2.FutureOr<void> init(_i1.GetItHelper gh) {
    final networkModule = _$NetworkModule();
    gh.factory<_i3.HttpClient>(() => _i3.HttpClient());
    gh.lazySingleton<_i4.InternetConnectionChecker>(
        () => networkModule.internetConnectionChecker());
    gh.lazySingleton<_i5.NetworkVerify>(
        () => _i5.NetworkVerify(gh<_i4.InternetConnectionChecker>()));
    gh.factory<_i6.CatRepository>(() => _i7.CatHttpRepository(
          httpClient: gh<_i3.HttpClient>(),
          networkVerify: gh<_i5.NetworkVerify>(),
        ));
  }
}

class _$NetworkModule extends _i8.NetworkModule {}
