library infrastruture;

import 'package:injectable/injectable.dart';

export 'package:infrastructure/src/cat/exceptions/api_key_no_found.dart'
    show ApiKeyNoFoundException;
export 'package:infrastructure/src/shared/http_client/exceptions/http_client_exception.dart'
    show HttpClientException;
export 'package:infrastructure/src/shared/exceptions/not_connected_to_network.dart'
    show NotConnectedToNetworkException;

@InjectableInit.microPackage()
void initInfrastructure() {}
