import 'package:infrastructure/src/shared/exceptions/technical_exception.dart';

class HttpClientException extends TechnicalException {
  HttpClientException() : super(_httpclientExceptionMessage);

  static const _httpclientExceptionMessage =
      'Se produjo un error al intentar conectarse al servicio. Vuelva a intentarlo más tarde..';
}

