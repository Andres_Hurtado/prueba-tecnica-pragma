class NotConnectedToNetworkException implements Exception {
  NotConnectedToNetworkException({this.message = _notConnectedToNetwork});
  final String message;
  static const _notConnectedToNetwork = 'No conectado a internet';
  @override
  String toString() => message;
}
