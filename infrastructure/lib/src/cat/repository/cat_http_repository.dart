import 'dart:convert';

import 'package:domain/domain.dart';
import 'package:infrastructure/src/cat/anticorruption/cat_translator.dart';
import 'package:infrastructure/src/cat/environment/env.dart';
import 'package:infrastructure/src/cat/exceptions/api_key_no_found.dart';
import 'package:infrastructure/src/cat/http_response/dto/cat.dart';
import 'package:infrastructure/src/cat/http_response/dto/cat_image.dart';
import 'package:infrastructure/src/cat/contant/cat_contant.dart';
import 'package:infrastructure/src/shared/exceptions/not_connected_to_network.dart';
import 'package:infrastructure/src/shared/http_client/http_client.dart';
import 'package:infrastructure/src/shared/network/network_verify.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: CatRepository)
class CatHttpRepository implements CatRepository {
  CatHttpRepository({
    required HttpClient httpClient,
    required NetworkVerify networkVerify,
  })  : _httpClient = httpClient,
        _networkVerify = networkVerify {
    if (Env.api_url.isEmpty && Env.api_key.isEmpty) {
      throw ApiKeyNoFoundException();
    }
  }

  final HttpClient _httpClient;
  final NetworkVerify _networkVerify;
  final apiUrl = Env.api_url;
  final apiUrlImage = Env.api_url_image;
  final headers = {
    "Content-Type": "application/json",
    "x-api-key": Env.api_key
  };

  @override
  Future<List<CatModel>> get(int page) async {
    if (await _networkVerify.isConnected) {
      final url = Uri.parse('$apiUrl' + '?limit=$FETCH_LIMIT&page=$page');
      final response = await _httpClient.getRequest(url, headers: headers);
      final listMapCat = List<Map<String, dynamic>>.from(json.decode(response));
      final resultCats = listMapCat.map(Cat.fromJson).toList();
      List<CatModel> cats = [];
      for (var cat in resultCats) {
        String referenceImage = '';
        if (cat.referenceImageId != null && cat.referenceImageId!.isNotEmpty) {
          referenceImage = await getImageUrl(cat.referenceImageId!);
        }
        final rCat = CatTranslator.fromDtoToDomain(
          cat: cat,
          referenceImage: referenceImage,
        );
        if (!cats.contains(rCat)) cats.add(rCat);
      }
      return cats;
    } else {
      throw NotConnectedToNetworkException();
    }
  }

  @override
  Future<CatModel?> search(String razaId) async {
    if (await _networkVerify.isConnected) {
      final url = Uri.parse('$apiUrl/$razaId');
      final response = await _httpClient.getRequest(url, headers: headers);
      if (response.isEmpty) return null;
      final jsonData = json.decode(response);
      if (jsonData == null) return null;
      final cat = Cat.fromJson(jsonData);
      String referenceImage = '';
      if (cat.referenceImageId != null && cat.referenceImageId!.isNotEmpty) {
        referenceImage = await getImageUrl(cat.referenceImageId!);
      }
      return CatTranslator.fromDtoToDomain(
        cat: cat,
        referenceImage: referenceImage,
      );
    } else {
      throw NotConnectedToNetworkException();
    }
  }

  Future<String> getImageUrl(String reference) async {
    try {
      final url = Uri.parse('$apiUrlImage/$reference');
      final response = await _httpClient.getRequest(url, headers: headers);
      if (response.isEmpty) return '';
      return CatImage.fromJson(json.decode(response)).url;
    } catch (e) {
      return '';
    }
  }
}
