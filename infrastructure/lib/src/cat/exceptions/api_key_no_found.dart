import 'package:infrastructure/src/shared/exceptions/technical_exception.dart';

class ApiKeyNoFoundException extends TechnicalException {
  ApiKeyNoFoundException() : super(_apiKeyNoFoundExceptionMessage);
  static const _apiKeyNoFoundExceptionMessage = 'Error interno comunicate con el administrador.';
}

