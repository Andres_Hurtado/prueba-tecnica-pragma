import 'package:infrastructure/src/cat/http_response/dto/cat_weight.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cat.g.dart';

@JsonSerializable()
class Cat {
  Cat({
    required this.weight,
    required this.id,
    required this.name,
    this.cfaUrl,
    this.vetstreetUrl,
    this.vcahospitalsUrl,
    required this.temperament,
    required this.origin,
    required this.countryCodes,
    required this.countryCode,
    required this.description,
    required this.lifeSpan,
    required this.indoor,
    this.lap,
    this.altNames,
    required this.adaptability,
    required this.affectionLevel,
    required this.childFriendly,
    required this.dogFriendly,
    required this.energyLevel,
    required this.grooming,
    required this.healthIssues,
    required this.intelligence,
    required this.sheddingLevel,
    required this.socialNeeds,
    required this.strangerFriendly,
    required this.vocalisation,
    required this.experimental,
    required this.hairless,
    required this.natural,
    required this.rare,
    required this.rex,
    required this.suppressedTail,
    required this.shortLegs,
    this.wikipediaUrl,
    required this.hypoallergenic,
    this.referenceImageId,
    this.catFriendly,
    this.bidability,
  });
  factory Cat.fromJson(Map<String, dynamic> json) => _$CatFromJson(json);

  @JsonKey(toJson: _catWeightToJson)
  CatWeight weight;
  String id;
  String name;
  @JsonKey(name: 'cfa_url')
  String? cfaUrl;
  @JsonKey(name: 'vetstreet_url')
  String? vetstreetUrl;
  @JsonKey(name: 'vcahospitals_url')
  String? vcahospitalsUrl;
  String temperament;
  String origin;
  @JsonKey(name: 'country_codes')
  String countryCodes;
  @JsonKey(name: 'country_code')
  String countryCode;
  String description;
  @JsonKey(name: 'life_span')
  String lifeSpan;
  int indoor;
  int? lap;
  @JsonKey(name: 'alt_names')
  String? altNames;
  int adaptability;
  @JsonKey(name: 'affection_level')
  int affectionLevel;
  @JsonKey(name: 'child_friendly')
  int childFriendly;
  @JsonKey(name: 'dog_friendly')
  int dogFriendly;
  @JsonKey(name: 'energy_level')
  int energyLevel;
  int grooming;
  @JsonKey(name: 'health_issues')
  int healthIssues;
  int intelligence;
  @JsonKey(name: 'shedding_level')
  int sheddingLevel;
  @JsonKey(name: 'social_needs')
  int socialNeeds;
  @JsonKey(name: 'stranger_friendly')
  int strangerFriendly;
  int vocalisation;
  int experimental;
  int hairless;
  int natural;
  int rare;
  int rex;
  @JsonKey(name: 'suppressed_tail')
  int suppressedTail;
  @JsonKey(name: 'short_legs')
  int shortLegs;
  @JsonKey(name: 'wikipedia_url')
  String? wikipediaUrl;
  int hypoallergenic;
  @JsonKey(name: 'reference_image_id')
  String? referenceImageId;
  @JsonKey(name: 'cat_friendly')
  int? catFriendly;
  int? bidability;
  Map<String, dynamic> toJson() => _$CatToJson(this);
  static Map<String, dynamic> _catWeightToJson(CatWeight value) =>
      value.toJson();
}
