import 'package:json_annotation/json_annotation.dart';

part 'cat_image.g.dart';

@JsonSerializable()
class CatImage {
  CatImage({
    required this.url,
  });
  factory CatImage.fromJson(Map<String, dynamic> json) =>
      _$CatImageFromJson(json);

  String url;
  Map<String, dynamic> toJson() => _$CatImageToJson(this);
}
