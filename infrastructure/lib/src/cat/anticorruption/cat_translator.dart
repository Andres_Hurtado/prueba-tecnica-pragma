import 'package:domain/domain.dart';
import 'package:infrastructure/src/cat/http_response/dto/cat.dart';

class CatTranslator {
  static CatModel fromDtoToDomain(
      {required Cat cat, required String referenceImage}) {
    return CatModel(
      catWeightModel: CatWeightModel(
        imperial: cat.weight.imperial,
        metric: cat.weight.metric,
      ),
      id: cat.id,
      name: cat.name,
      temperament: cat.temperament,
      origin: cat.origin,
      countryCodes: cat.countryCodes,
      countryCode: cat.countryCode,
      description: cat.description,
      lifeSpan: cat.lifeSpan,
      indoor: cat.indoor,
      adaptability: cat.adaptability,
      affectionLevel: cat.affectionLevel,
      childFriendly: cat.childFriendly,
      dogFriendly: cat.dogFriendly,
      energyLevel: cat.energyLevel,
      grooming: cat.grooming,
      healthIssues: cat.healthIssues,
      intelligence: cat.intelligence,
      sheddingLevel: cat.sheddingLevel,
      socialNeeds: cat.socialNeeds,
      strangerFriendly: cat.strangerFriendly,
      vocalisation: cat.vocalisation,
      experimental: cat.experimental,
      hairless: cat.hairless,
      natural: cat.natural,
      rare: cat.rare,
      rex: cat.rex,
      suppressedTail: cat.suppressedTail,
      shortLegs: cat.shortLegs,
      hypoallergenic: cat.hypoallergenic,
      referenceImage: referenceImage,
    );
  }
}
