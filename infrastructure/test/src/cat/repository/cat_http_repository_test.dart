import 'package:domain/domain.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:infrastructure/src/cat/repository/cat_http_repository.dart';
import 'package:infrastructure/src/shared/http_client/http_client.dart';
import 'package:infrastructure/src/shared/network/network_verify.dart';
import 'package:mocktail/mocktail.dart';

import '../../../test_data_builder/cat.dart';

class MockNetworkVerify extends Mock implements NetworkVerify {}

class MockHttpClient extends Mock implements HttpClient {}

void main() {
  late CatHttpRepository catHttpRepository;
  late HttpClient httpClient;
  late NetworkVerify networkVerify;

  final cat = CatBuilder.from()
      .withId('abys')
      .withName('Abyssinian')
      .withCfaUrl('http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx')
      .withVetstreetUrl('http://www.vetstreet.com/cats/abyssinian')
      .withVcahospitalsUrl(
          'https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian')
      .withTemperament('Active, Energetic, Independent, Intelligent, Gentle')
      .withOrigin('Egypt')
      .withCountryCodes('EG')
      .withCountryCode('EG')
      .withDescription(
          'he Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.')
      .withLifeSpan('14 - 15')
      .withIndoor(0)
      .withLap(1)
      .withAltNames('')
      .withAdaptability(5)
      .withAffectionLevel(5)
      .withChildFriendly(3)
      .withDogFriendly(4)
      .withEnergyLevel(5)
      .withGrooming(1)
      .withHealthIssues(2)
      .withIntelligence(5)
      .withSheddingLevel(2)
      .withSocialNeeds(5)
      .withStrangerFriendly(5)
      .withVocalisation(1)
      .withExperimental(0)
      .withHairless(0)
      .withNatural(1)
      .withRare(0)
      .withRex(0)
      .withSuppressedTail(0)
      .withShortLegs(0)
      .withWikipediaUrl('https://en.wikipedia.org/wiki/Abyssinian_(cat)')
      .withHypoallergenic(0)
      .withReferenceImage('url')
      .withImperial('7  -  10')
      .withMetric('3 - 5')
      .build();

  setUp(() {
    networkVerify = MockNetworkVerify();
    httpClient = HttpClient();
    catHttpRepository = CatHttpRepository(
      httpClient: httpClient,
      networkVerify: networkVerify,
    );
  });

  group('Cat Services', () {
    test('catServices_whenIsInvokedGet_getResponseSuccess', () async {
      //Arrange
      when(() => networkVerify.isConnected).thenAnswer((_) async => true);
      //Act
      final result = await catHttpRepository.get(1);
      //Assert
      verify(() => networkVerify.isConnected);
      expect(result, isA<List<CatModel>>());
    });
    test('catServices_whenIsInvokedSearchCatBreed_searchResponseSuccess',
        () async {
      //Arrange
      const razaId = 'abys';
      when(() => networkVerify.isConnected).thenAnswer((_) async => true);
      //Act
      final result = await catHttpRepository.search(razaId);
      //Assert
      verify(() => networkVerify.isConnected);
      expect(result, isA<CatModel>());
      expect(cat.id, result?.id);
    });
  });
}
