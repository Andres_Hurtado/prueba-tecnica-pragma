import 'package:domain/domain.dart';
import 'package:domain/src/cat/services/cat_services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../test_data_builder/cat.dart';

class MockCatServices extends Mock implements CatServices {}

void main() {
  late CatServices catServices;
  final cat = CatBuilder.from()
      .withId('abys')
      .withName('Abyssinian')
      .withCfaUrl('http://cfa.org/Breeds/BreedsAB/Abyssinian.aspx')
      .withVetstreetUrl('http://www.vetstreet.com/cats/abyssinian')
      .withVcahospitalsUrl(
          'https://vcahospitals.com/know-your-pet/cat-breeds/abyssinian')
      .withTemperament('Active, Energetic, Independent, Intelligent, Gentle')
      .withOrigin('Egypt')
      .withCountryCodes('EG')
      .withCountryCode('EG')
      .withDescription(
          'he Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.')
      .withLifeSpan('14 - 15')
      .withIndoor(0)
      .withLap(1)
      .withAltNames('')
      .withAdaptability(5)
      .withAffectionLevel(5)
      .withChildFriendly(3)
      .withDogFriendly(4)
      .withEnergyLevel(5)
      .withGrooming(1)
      .withHealthIssues(2)
      .withIntelligence(5)
      .withSheddingLevel(2)
      .withSocialNeeds(5)
      .withStrangerFriendly(5)
      .withVocalisation(1)
      .withExperimental(0)
      .withHairless(0)
      .withNatural(1)
      .withRare(0)
      .withRex(0)
      .withSuppressedTail(0)
      .withShortLegs(0)
      .withWikipediaUrl('https://en.wikipedia.org/wiki/Abyssinian_(cat)')
      .withHypoallergenic(0)
      .withReferenceImage('url')
      .withImperial('7  -  10')
      .withMetric('3 - 5')
      .build();

  setUp(() {
    catServices = MockCatServices();
  });

  group('Cat Services', () {
    test('catServices_whenIsInvokedGetCatBreeds_getResponseSuccess', () async {
      //Arrange
      const page = 1;
      when(() => catServices.getCatBreeds(page)).thenAnswer((_) async => [cat]);
      //Act
      final result = await catServices.getCatBreeds(page);
      //Assert
      verify(() => catServices.getCatBreeds(page)).called(1);
      expect([cat], result);
    });
    test('catServices_whenIsInvokedSearchCatBreed_searchResponseSuccess',
        () async {
      //Arrange
      const razaName = 'Abyssinian';
      when(() => catServices.searchCatBreed(any()))
          .thenAnswer((_) async => cat);
      //Act
      final result = await catServices.searchCatBreed(razaName);
      //Assert
      verify(() => catServices.searchCatBreed(razaName)).called(1);
      expect(cat, result);
    });
  });
}
