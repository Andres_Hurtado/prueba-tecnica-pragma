// ignore_for_file: avoid_returning_this

import 'package:domain/domain.dart';

class CatBuilder {
  late String _id;
  late String _name;
  String? _cfaUrl;
  String? _vetstreetUrl;
  String? _vcahospitalsUrl;
  late String _temperament;
  late String _origin;
  late String _countryCodes;
  late String _countryCode;
  late String _description;
  late String _lifeSpan;
  late int _indoor;
  int? _lap;
  String? _altNames;
  late int _adaptability;
  late int _affectionLevel;
  late int _childFriendly;
  late int _dogFriendly;
  late int _energyLevel;
  late int _grooming;
  late int _healthIssues;
  late int _intelligence;
  late int _sheddingLevel;
  late int _socialNeeds;
  late int _strangerFriendly;
  late int _vocalisation;
  late int _experimental;
  late int _hairless;
  late int _natural;
  late int _rare;
  late int _rex;
  late int _suppressedTail;
  late int _shortLegs;
  String? _wikipediaUrl;
  late int _hypoallergenic;
  String? _referenceImage;
  int? _bidability;
  int? _catFriendly;
  late String _imperial;
  late String _metric;

  static CatBuilder from() => CatBuilder();

  CatBuilder withId(String id) {
    _id = id;
    return this;
  }

  CatBuilder withName(String name) {
    _name = name;
    return this;
  }

  CatBuilder withCfaUrl(String? cfaUrl) {
    _cfaUrl = cfaUrl;
    return this;
  }

  CatBuilder withVetstreetUrl(String? vetstreetUrl) {
    _vetstreetUrl = vetstreetUrl;
    return this;
  }

  CatBuilder withVcahospitalsUrl(String? vcahospitalsUrl) {
    _vcahospitalsUrl = vcahospitalsUrl;
    return this;
  }

  CatBuilder withTemperament(String temperament) {
    _temperament = temperament;
    return this;
  }

  CatBuilder withLifeSpan(String lifeSpan) {
    _lifeSpan = lifeSpan;
    return this;
  }

  CatBuilder withOrigin(String origin) {
    _origin = origin;
    return this;
  }

  CatBuilder withCountryCodes(String countryCodes) {
    _countryCodes = countryCodes;
    return this;
  }

  CatBuilder withDescription(String description) {
    _description = description;
    return this;
  }

  CatBuilder withCountryCode(String countryCode) {
    _countryCode = countryCode;
    return this;
  }

  CatBuilder withIndoor(int indoor) {
    _indoor = indoor;
    return this;
  }

  CatBuilder withLap(int? lap) {
    _lap = lap;
    return this;
  }

  CatBuilder withAltNames(String? altNames) {
    _altNames = altNames;
    return this;
  }

  CatBuilder withAdaptability(int adaptability) {
    _adaptability = adaptability;
    return this;
  }

  CatBuilder withAffectionLevel(int affectionLevel) {
    _affectionLevel = affectionLevel;
    return this;
  }

  CatBuilder withChildFriendly(int childFriendly) {
    _childFriendly = childFriendly;
    return this;
  }

  CatBuilder withDogFriendly(int dogFriendly) {
    _dogFriendly = dogFriendly;
    return this;
  }

  CatBuilder withEnergyLevel(int energyLevel) {
    _energyLevel = energyLevel;
    return this;
  }

  CatBuilder withGrooming(int grooming) {
    _grooming = grooming;
    return this;
  }

  CatBuilder withHealthIssues(int healthIssues) {
    _healthIssues = healthIssues;
    return this;
  }

  CatBuilder withIntelligence(int intelligence) {
    _intelligence = intelligence;
    return this;
  }

  CatBuilder withSheddingLevel(int sheddingLevel) {
    _sheddingLevel = sheddingLevel;
    return this;
  }

  CatBuilder withSocialNeeds(int socialNeeds) {
    _socialNeeds = socialNeeds;
    return this;
  }

  CatBuilder withStrangerFriendly(int strangerFriendly) {
    _strangerFriendly = strangerFriendly;
    return this;
  }

  CatBuilder withVocalisation(int vocalisation) {
    _vocalisation = vocalisation;
    return this;
  }

  CatBuilder withExperimental(int experimental) {
    _experimental = experimental;
    return this;
  }

  CatBuilder withHairless(int hairless) {
    _hairless = hairless;
    return this;
  }

  CatBuilder withNatural(int natural) {
    _natural = natural;
    return this;
  }

  CatBuilder withRare(int rare) {
    _rare = rare;
    return this;
  }

  CatBuilder withRex(int rex) {
    _rex = rex;
    return this;
  }

  CatBuilder withSuppressedTail(int suppressedTail) {
    _suppressedTail = suppressedTail;
    return this;
  }

  CatBuilder withShortLegs(int shortLegs) {
    _shortLegs = shortLegs;
    return this;
  }

  CatBuilder withWikipediaUrl(String? wikipediaUrl) {
    _wikipediaUrl = wikipediaUrl;
    return this;
  }

  CatBuilder withHypoallergenic(int hypoallergenic) {
    _hypoallergenic = hypoallergenic;
    return this;
  }

  CatBuilder withReferenceImage(String? referenceImage) {
    _referenceImage = referenceImage;
    return this;
  }

  CatBuilder withBidability(int? bidability) {
    _bidability = bidability;
    return this;
  }

  CatBuilder withCatFriendly(int? catFriendly) {
    _catFriendly = catFriendly;
    return this;
  }

  CatBuilder withImperial(String imperial) {
    _imperial = imperial;
    return this;
  }

  CatBuilder withMetric(String metric) {
    _metric = metric;
    return this;
  }

  CatModel build() => CatModel(
        id: _id,
        name: _name,
        cfaUrl: _cfaUrl,
        vetstreetUrl: _vetstreetUrl,
        vcahospitalsUrl: _vcahospitalsUrl,
        description: _description,
        temperament: _temperament,
        origin: _origin,
        countryCodes: _countryCodes,
        countryCode: _countryCode,
        lifeSpan: _lifeSpan,
        indoor: _indoor,
        lap: _lap,
        altNames: _altNames,
        adaptability: _adaptability,
        affectionLevel: _affectionLevel,
        childFriendly: _childFriendly,
        dogFriendly: _dogFriendly,
        energyLevel: _energyLevel,
        grooming: _grooming,
        healthIssues: _healthIssues,
        intelligence: _intelligence,
        sheddingLevel: _sheddingLevel,
        socialNeeds: _socialNeeds,
        strangerFriendly: _strangerFriendly,
        vocalisation: _vocalisation,
        experimental: _experimental,
        hairless: _hairless,
        natural: _natural,
        rare: _rare,
        rex: _rex,
        suppressedTail: _suppressedTail,
        shortLegs: _shortLegs,
        wikipediaUrl: _wikipediaUrl,
        hypoallergenic: _hypoallergenic,
        referenceImage: _referenceImage,
        bidability: _bidability,
        catFriendly: _catFriendly,
        catWeightModel: CatWeightModel(
          imperial: _imperial,
          metric: _metric,
        ),
      );
}
