import 'package:domain/src/cat/model/cat_model.dart';

abstract interface class CatRepository {
  Future<List<CatModel>> get(int page);
  Future<CatModel?> search(String razaIdName);
}
