import 'package:domain/domain.dart';
import 'package:injectable/injectable.dart';

@injectable
class CatServices {
  CatServices({required CatRepository catRepository})
      : _catRepository = catRepository;

  final CatRepository _catRepository;

  Future<List<CatModel>> getCatBreeds(int page) => _catRepository.get(page);

  Future<CatModel?> searchCatBreed(String razaIdName) => razaIdName.isNotEmpty
      ? _catRepository.search(razaIdName)
      : throw SearchEmptyException();
}
