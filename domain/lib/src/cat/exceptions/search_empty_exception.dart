const _searchEmptyMessage = 'Error, debes de escribir el nombre de una raza para buscar';

class SearchEmptyException implements Exception {
  SearchEmptyException({this.message = _searchEmptyMessage});
  final String message;
}
