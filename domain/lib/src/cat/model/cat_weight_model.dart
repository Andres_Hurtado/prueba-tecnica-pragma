class CatWeightModel {
  const CatWeightModel({
    required this.imperial,
    required this.metric,
  });

  final String imperial;
  final String metric;
}
