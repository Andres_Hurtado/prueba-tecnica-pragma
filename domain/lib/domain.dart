library domain;

import 'package:injectable/injectable.dart';

export 'package:domain/src/cat/exceptions/search_empty_exception.dart'
    show SearchEmptyException;
export 'package:domain/src/cat/model/cat_model.dart' show CatModel;
export 'package:domain/src/cat/model/cat_weight_model.dart' show CatWeightModel;
export 'package:domain/src/cat/repository/cat_repository.dart'
    show CatRepository;
export 'package:domain/src/cat/services/cat_services.dart' show CatServices;

@InjectableInit.microPackage()
void initDomain() {}
